<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;

class APIController extends Controller
{

    protected $response = [];

    public function __construct()
    {
        $this->middleware('api_key');
    }

    public function countries() {
        $countries = Country::all();
        $this->generateResponse();
        return json_encode($this->response);
    }

    private function generateResponse(){
        $countries = Country::all();

        $this->response = [
            'status' => 200,
            'data'  => $countries
        ];
    }
}
