<?php

namespace App\Http\Middleware;

use Closure;

class CheckAPIKey
{

    protected $api_key = '';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('api_key')) {
            $this->api_key = $request->input('api_key');

            return $this->reroute($request,$next);

        }else
            return response('Bad request.',403);
        
    }

    private function reroute($request, $next){
        if($this->validateKey())
            return $next($request);
        else
            return response('Unauthorized.', 401);
    }

    private function validateKey(){
        return $this->api_key == 'api_key';
    }
}
