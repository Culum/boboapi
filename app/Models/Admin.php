<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public function users(){
        return $this->morphOne('App\Models\User', 'userable');
    }
}
