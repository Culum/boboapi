<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Admin;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        Student::truncate();
        Teacher::truncate();
        Admin::truncate();

        // Create student user 
        $student = Student::create([

        ]);

        $user = $student->users()->create([
            'name' => 'student',
            'email' => 'zoran.culum88@gmail.com',
            'password' => Hash::make('jebaoja')
        ]);
        
        // Create teacher user
        $teacher = Teacher::create([

        ]);

        $user = $teacher->users()->create([
            'name' => 'teacher',
            'email' => 'zoran.culum@gmail.com',
            'password' => Hash::make('jebaoja')
        ]);
    

    }
}
